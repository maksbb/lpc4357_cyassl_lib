/* cyassl options.h
 * generated from configure options
 *
 * Copyright (C) 2006-2014 wolfSSL Inc.
 *
 * This file is part of CyaSSL.
 *
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#undef  _POSIX_THREADS
#define _POSIX_THREADS

#undef  DEBUG
#define DEBUG

#undef  DEBUG_CYASSL
#define DEBUG_CYASSL

#undef  HAVE_THREAD_LS
#define HAVE_THREAD_LS

#undef  NO_DSA
#define NO_DSA

#undef  NO_PSK
#define NO_PSK

#undef  NO_DH
#define NO_DH

#undef  NO_MD4
#define NO_MD4

#undef  NO_PWDBASED
#define NO_PWDBASED

#undef  NO_HC128
#define NO_HC128

#undef  NO_RABBIT
#define NO_RABBIT

#undef  USE_FAST_MATH
#define USE_FAST_MATH


#ifdef __cplusplus
}
#endif

