################################################################################
# Build the core FreeRTOS as a static lib
################################################################################

TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
BIN  = $(CP) -O ihex
AR   = $(TRGT)ar

MCU = cortex-m4
FPU = -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -D__FPU_USED=1

MCFLAGS = -mthumb -mcpu=$(MCU) $(FPU)

# Define optimisation level here
OPT = -Os

CFLAGS=-DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M4 -D__MULTICORE_NONE -D__NEWLIB__ \
  -I"inc" -I"." -I"../lpc_chip_43xx/inc" -I"../lpc_board_ea_oem_4357/inc" -I"../lpc4357_freertos_lib/inc" \
  -I"../lpc4357_lwip_lib/inc" -I"../lpc4357_lwip_lib/inc/ipv4" \
  -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fsingle-precision-constant -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -mthumb -D__NEWLIB__ -specs=redlib.specs -MMD -MP
LDFLAGS=-lrt



# Add inputs and outputs from these tool invocations to the build variables
SOURCES = \
src/crl.c \
src/internal.c \
src/io.c \
src/keys.c \
src/ocsp.c \
src/sniffer.c \
src/ssl.c \
src/tls.c

SOURCES += \
ctaocrypt/src/aes.c \
ctaocrypt/src/arc4.c \
ctaocrypt/src/asm.c \
ctaocrypt/src/asn.c \
ctaocrypt/src/blake2b.c \
ctaocrypt/src/camellia.c \
ctaocrypt/src/chacha.c \
ctaocrypt/src/coding.c \
ctaocrypt/src/compress.c \
ctaocrypt/src/des3.c \
ctaocrypt/src/dh.c \
ctaocrypt/src/dsa.c \
ctaocrypt/src/ecc.c \
ctaocrypt/src/ecc_fp.c \
ctaocrypt/src/error.c \
ctaocrypt/src/fips.c \
ctaocrypt/src/fips_test.c \
ctaocrypt/src/hc128.c \
ctaocrypt/src/hmac.c \
ctaocrypt/src/integer.c \
ctaocrypt/src/logging.c \
ctaocrypt/src/md2.c \
ctaocrypt/src/md4.c \
ctaocrypt/src/md5.c \
ctaocrypt/src/memory.c \
ctaocrypt/src/misc.c \
ctaocrypt/src/pkcs7.c \
ctaocrypt/src/poly1305.c \
ctaocrypt/src/pwdbased.c \
ctaocrypt/src/rabbit.c \
ctaocrypt/src/random.c \
ctaocrypt/src/ripemd.c \
ctaocrypt/src/rsa.c \
ctaocrypt/src/sha.c \
ctaocrypt/src/sha256.c \
ctaocrypt/src/sha512.c \
ctaocrypt/src/tfm.c \
ctaocrypt/src/wc_port.c \
ctaocrypt/src/wolfcrypt_first.c \
ctaocrypt/src/wolfcrypt_last.c

OBJECTS=$(SOURCES:.c=.o)

CYASSL_LIB=libcyassl_lpc4357.a

all: $(SOURCES) $(CYASSL_LIB)

$(CYASSL_LIB): $(OBJECTS)
	$(AR) $(ARFLAGS) $(CYASSL_LIB)  $?

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f src/*.o ctaocrypt/src/*.o $(CYASSL_LIB)


